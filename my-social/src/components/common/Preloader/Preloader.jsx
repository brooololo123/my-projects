import React from "react";
import preloader from '../../../assect/Settings.gif'

const Preloader = () => {
    return <div>
        <img src={preloader} alt=""/>
    </div>
}

export default Preloader;