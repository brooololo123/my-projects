import React from "react";
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import {Route, withRouter} from "react-router-dom";
import Footer from "./components/Footer/Footer";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import {connect} from "react-redux";
import UsersContainer from "./components/Users/UsersContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import Login from "./components/Login/Login";
import { InitializedThunk } from "./Redux/appReducer";
import Preloader from "./components/common/Preloader/Preloader";
import { compose } from "redux";

class App extends React.Component {

    componentDidMount () {
        this.props.InitializedThunk()
    }
    render () {

        if(!this.props.initialized) {
            return <Preloader />
        }
        
        return (
                    <div className='containerApp'>
                        <HeaderContainer/>
                        <Navbar/>
                        <div className='containerContent'>
                            <Route path='/profile/:userId?' render={() => <ProfileContainer/>}/>
                            <Route path='/dialogs' render={() => <DialogsContainer/>}/>
                            <Route path='/users' render={() => <UsersContainer/>}/>
                            <Route path='/login' render={() => <Login />}/>
                        </div>
                        <Footer/>
                    </div>
        )

    }
    
}

const mapStateToProps = (state) => ({
initialized: state.app.initialized
})

export default compose (
    withRouter,
connect(mapStateToProps, {InitializedThunk})
)(App)
