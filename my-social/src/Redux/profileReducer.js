import { usersAPI, profileAPI } from "../api/api";

const ADD_POST = 'ADD-POST';
const NEW_POST_TEXT = 'NEW-POST-TEXT';
const NEW_USERS_PROFILE = 'NEW_USERS_PROFILE';
const GET_STATUS = 'GET_STATUS';
const UPDATE_STATUS = 'UPDATE_STATUS';

export const addPost = (post) => ({ type: ADD_POST, post })
export const newPostText = (text) => ({ type: NEW_POST_TEXT, text: text })
export const newUsersProfile = (profile) => ({ type: NEW_USERS_PROFILE, profile })
export const getStatus = (status) => ({ type: GET_STATUS, status })
export const updateStatus = (status) => ({ type: UPDATE_STATUS, status })

export const usersProfileThunk = (userId) => {
    return (dispatch) => {
        usersAPI.getUsersProfile(userId)
            .then((data) => {
                dispatch(newUsersProfile(data))
            })
    }
}

export const getStatusThunk = (userId) => {
    return (dispatch) => {

        profileAPI.getStatus(userId)
            .then((data) => {
                dispatch(getStatus(data))
            })
    }
}

export const updateStatusThunk = (status) => {
    return (dispatch) => {
        profileAPI.updateStatus(status)
            .then((data) => {
                if (data.data.resultCode === 0) {
                    dispatch(updateStatus(status))
                }
            })
    }
}

let initialState = {
    posts: [
        { message: 'Привет React', likeCount: 34, id: 0 },
        { message: 'Это мое первое приложение', likeCount: 4, id: 1 },
        { message: 'Оу', likeCount: 10, id: 2 },
    ],
    newTextChange: '',
    profile: null,
    status: '',
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST: {
            let newPost = {
                message: action.post,
                likeCount: 0,
                id: state.posts.length
            }
            return {
                ...state,
                posts: [...state.posts, newPost]
            }
        }
        case NEW_USERS_PROFILE: {
            return {
                ...state,
                profile: action.profile
            }
        }
        case GET_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        case UPDATE_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        default: return state;
    }
}

export default profileReducer;